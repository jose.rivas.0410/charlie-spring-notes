package com.codebound.charliespringnotes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CharlieSpringNotesApplication {

    public static void main(String[] args) {
        SpringApplication.run(CharlieSpringNotesApplication.class, args);
    }

}
