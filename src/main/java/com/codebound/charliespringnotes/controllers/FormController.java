package com.codebound.charliespringnotes.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

@Controller
public class FormController {

    @GetMapping("/show-form")
    public String showForm() {
        return "company-form";
    }

    @GetMapping("/process-form")
    public String processForm(@RequestParam Map<String, String> requestParams, Model model,@RequestParam(name = "firstName") String greeting) {
        model.addAttribute("fn", requestParams.get("firstName"));
        model.addAttribute("greeting", "Welcome " + greeting);
        model.addAttribute("ln", requestParams.get("lastName"));
        model.addAttribute("ct", requestParams.get("city"));
        model.addAttribute("st", requestParams.get("state"));
        model.addAttribute("em", requestParams.get("email"));
        return "show-form-submit";
    }
}
