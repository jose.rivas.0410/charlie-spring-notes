package com.codebound.charliespringnotes.controllers;

import com.codebound.charliespringnotes.models.Employee;
import com.codebound.charliespringnotes.repositories.EmployeeRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class EmployeeController {
    private final EmployeeRepository employeeRepo;

    public EmployeeController(EmployeeRepository employeeRepo) {
        this.employeeRepo = employeeRepo;
    }

    @GetMapping("/employees")
    public String index(Model model) {
        List<Employee> employeeList = employeeRepo.findAll();
        model.addAttribute("noEmployeeFound", employeeList.size() == 0);
        model.addAttribute("employees", employeeList);
        return "employees/index";
    }

    @GetMapping("/employees/add-employee")
    public String showForm(Model viewModel) {
        viewModel.addAttribute("employee", new Employee());
        return "employees/add-employee";
    }

    @PostMapping("/employees/add-employee")
    public String newEmployee(@ModelAttribute Employee employeeToBeAdded) {
        employeeRepo.save(employeeToBeAdded);
        return "redirect:/employees";
    }

    @GetMapping("/employees/{id}")
    public String showEmployee(@PathVariable int id, Model model) {
        Employee employee = employeeRepo.getOne(id);
        model.addAttribute("employeeId", id);
        model.addAttribute("employee", employee);
        return "/employees/show";
    }

    @GetMapping("/employees/{id}/edit")
    public String showEditEmployee(Model model, @PathVariable int id) {
        Employee editThisEmployee = employeeRepo.getOne(id);
        model.addAttribute("editEmployee", editThisEmployee);
        return "employees/edit";
    }

    @PostMapping("/employees/{id}/edit")
    public String update(@PathVariable int id,
                         @RequestParam(name = "employee_number") int employee_number,
                         @RequestParam(name = "first_name") String first_name,
                         @RequestParam(name = "last_name") String last_name,
                         @RequestParam(name = "email") String email,
                         @RequestParam(name = "username") String username
                        ) {
        Employee foundEmployee = employeeRepo.getOne(id);
        foundEmployee.setEmployee_number(employee_number);
        foundEmployee.setFirst_name(first_name);
        foundEmployee.setLast_name(last_name);
        foundEmployee.setEmail(email);
        foundEmployee.setUsername(username);
        employeeRepo.save(foundEmployee);
        return "redirect:/employees";
    }

    @PostMapping("/employees/{id}/delete")
    public String remove(@PathVariable int id) {
        employeeRepo.deleteById(id);
        return "redirect:/employees";
    }

}
