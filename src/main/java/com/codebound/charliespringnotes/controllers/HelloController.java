package com.codebound.charliespringnotes.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class HelloController {

    @GetMapping("/hello/{user}")
    @ResponseBody
    public String name(@PathVariable String user) {
        return "Hello " + user + "!";
    }
}
