package com.codebound.charliespringnotes.controllers;

import com.codebound.charliespringnotes.models.Ad;
import com.codebound.charliespringnotes.repositories.AdRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class AdController {
    // these next two steps: dependency injection

    private final AdRepository adRepo;


    public AdController(AdRepository adRepo) {
        this.adRepo = adRepo;
    }

    // created a Repository instance and initialized it
    // in the constructor class

    @GetMapping("/ads")
    public String index(Model model) {
        List<Ad> adList = adRepo.findAll();

        model.addAttribute("noAdsFound", adList.size() == 0);
        model.addAttribute("ads", adList);

        return "ads/index";
    }

    // show the form to add a new ad
    @GetMapping("/ads/create")
    public String showForm(Model viewModel) {
        viewModel.addAttribute("ad", new Ad());
        return "ads/create";
    }

    // create a new ad
    @PostMapping("/ads/create")
    public String caveNewAd(@ModelAttribute Ad adToBeSaved) {
        adRepo.save(adToBeSaved);

        return "redirect:/ads";
    }

    // controller method to display the individual ad
    @GetMapping("/ads/{id}")
    public String showAd(@PathVariable long id, Model model) {
        Ad ad = adRepo.getOne(id);

        model.addAttribute("adId", id);
        model.addAttribute("ad", ad);

        return "/ads/show";
    }

    // controller method that will allow for our edit function
    @GetMapping("/ads/{id}/edit")
    public String showEditForm(Model model, @PathVariable long id) {
        // find the id
        Ad editThisAd = adRepo.getOne(id);
        model.addAttribute("adEdit", editThisAd);

        return "ads/edit";
    }

    @PostMapping("/ads/{id}/edit")
    @ResponseBody
    public String update(@PathVariable long id,
                         @RequestParam(name = "title") String title,
                         @RequestParam(name = "title") String description
                        ) {
        // find the ad
        Ad foundAd = adRepo.getOne(id); // select *(everything) from ads where id =
        // edit the ad
        foundAd.setTitle(title);
        foundAd.setDescription(description);
        // save the new ad's changes
        adRepo.save(foundAd);
        return "Ad Updated";
    }

    // controller method to delete an ad
    @PostMapping("/ads/{id}/delete")
    @ResponseBody
    public String destroy(@PathVariable long id) {
        adRepo.deleteById(id);
        return "Ad Deleted";
    }

}
