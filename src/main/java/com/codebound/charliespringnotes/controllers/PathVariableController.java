package com.codebound.charliespringnotes.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class PathVariableController {

    @GetMapping("/greeting/{name}")
    @ResponseBody
    public String greeting(@PathVariable String name) {
        return "Greetings " + name + "!";
    }

    /*
    NOTES:
    Spring allows us to use path variables. That is variables that are a part of the URL of a request,
    as opposed to being passed as a query string, or as part of the request body.
     */
}
