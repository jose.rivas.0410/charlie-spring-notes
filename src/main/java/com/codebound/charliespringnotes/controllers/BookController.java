package com.codebound.charliespringnotes.controllers;

import com.codebound.charliespringnotes.models.Book;
import com.codebound.charliespringnotes.repositories.BookRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class BookController {
    private final BookRepository bookRepo;


    public BookController(BookRepository bookRepo) {
        this.bookRepo = bookRepo;
    }

    @GetMapping("/books")
    public String index(Model model) {
        List<Book> bookList = bookRepo.findAll();
        model.addAttribute("noBooksFound", bookList.size() == 0);
        model.addAttribute("books", bookList);
        return "books/index";
    }

    @GetMapping("/books/create")
    public String showForm(Model viewModel) {
        viewModel.addAttribute("book", new Book());
        return "books/create";
    }

    @PostMapping("/books/create")
    public String newBook(@ModelAttribute Book bookToBeSaved) {
        bookRepo.save(bookToBeSaved);
        return "redirect:/books";
    }

    @GetMapping("/books/{id}")
    public String showBook(@PathVariable long id, Model model) {
        Book book = bookRepo.getOne(id);
        model.addAttribute("bookId", id);
        model.addAttribute("book", book);
        return "/books/show";
    }

    @GetMapping("/books/{id}/edit")
    public String showEditBook(Model model, @PathVariable long id) {
        Book editThisBook = bookRepo.getOne(id);
        model.addAttribute("bookEdit", editThisBook);
        return "books/edit";
    }

    @PostMapping("/books/{id}/edit")
//    @ResponseBody
    public String update(@PathVariable long id,
                         @RequestParam(name = "title") String title,
                         @RequestParam(name = "author") String author,
                         @RequestParam(name = "genre") String genre,
                         @RequestParam(name = "date") String date
                        ) {
        Book foundBook = bookRepo.getOne(id);
        foundBook.setTitle(title);
        foundBook.setAuthor(author);
        foundBook.setGenre(genre);
        foundBook.setDate(date);
        bookRepo.save(foundBook);
        return "redirect:/books";
    }

    @PostMapping("/books/{id}/delete")
//    @ResponseBody
    public String remove(@PathVariable long id) {
        bookRepo.deleteById(id);
        return "redirect:/books";
    }

}
