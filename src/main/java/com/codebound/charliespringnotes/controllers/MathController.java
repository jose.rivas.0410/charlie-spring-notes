package com.codebound.charliespringnotes.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
public class MathController {

    @RequestMapping(path = "/add/13/and/7", method = RequestMethod.GET)
    @ResponseBody
    public String add() {
        return "13 + 7 = " + (13 + 7);
    }

    @GetMapping(path = "/subtract/13/from/20")
    @ResponseBody
    public String subtract() {
        return "20 - 7 = " + (20 - 7);
    }
}
