package com.codebound.charliespringnotes.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
public class HelloDataController {

    // Passing data to our view

    // define a controller method that accepts a model

    @GetMapping("/hello-data/{name}")
    public String helloName(@PathVariable String name, Model model) {
        model.addAttribute("person-name", name);
        return "hello-data-view";
    }

    // Model attributes are used inside controller classes that prepare the data for rendering inside a view
    // One way we can add attributes to our model is to require an instance of Model as a parameter
    // in a controller method

}
