package com.codebound.charliespringnotes.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
public class CasinoRollController {

    @GetMapping("/casino-roll")
    public String roll() {
        return "roll-view";
    }

    @GetMapping("/casino-roll/{num}")
    public String userGuess(@PathVariable int num, Model model) {
        int dice = (int)(Math.random()*(5) + 1);

        String message;

        if (dice == num) {
            message = "You guessed correctly";
        }else {
            message = "You guessed wrong";
        }

        model.addAttribute("message", message);

        return "roll-view";
    }
}
