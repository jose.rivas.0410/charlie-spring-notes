package com.codebound.charliespringnotes.repositories;

import com.codebound.charliespringnotes.models.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmployeeRepository extends JpaRepository<Employee, Integer> {
}
