package com.codebound.charliespringnotes.repositories;

/*
Repositories
- similar to DAOs,
- they are an abstraction of interacting with a database
- usually implements in Java using DAOs
    - to a predefined parent class
    named JpaRepository
 */

import com.codebound.charliespringnotes.models.Ad;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AdRepository extends JpaRepository<Ad, Long> {

}
