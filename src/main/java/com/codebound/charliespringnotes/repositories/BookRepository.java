package com.codebound.charliespringnotes.repositories;

import com.codebound.charliespringnotes.models.Book;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BookRepository extends JpaRepository<Book, Long> {
}
